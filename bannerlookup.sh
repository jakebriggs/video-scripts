#!/bin/bash

old_IFS=$IFS
IFS=$'\n'

files=( `ls -1 $1`)

IFS=$old_IFS

# Make some links
for f in "${files[@]}"
do
	#datas=`wget --quiet "http://www.omdbapi.com/?t=$f&r=XML" -O -`
	datas=`wget --quiet "http://thetvdb.com/api/GetSeries.php?seriesname=$f" -O -`

	POSTER=`echo $datas | xpath -q -e '/Data/Series/banner/text()'`

	echo "Found banner for $f - $POSTER"

	if [ "$POSTER" != "" ]
	  then
 	    echo $datas > "$1/$f/folder.xml"

     	    if [ ! -e "$1/$f/folder.jpg" ]
	     then
	      echo "$f/folder.jpg doesnt exist, getting from http://thetvdb.com/banners/$POSTER"
	      wget --quiet "http://thetvdb.com/banners/$POSTER" -O "$1/$f/folder.jpg"
	    fi

          else
            echo "Nothing found for $f"
        fi
done
