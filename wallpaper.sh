#!/bin/bash
while getopts :d:r: option
do
 case "${option}"
 in
 d) DIR=$OPTARG;;
 r) SUBREDDIT=$OPTARG;;
 esac
done

URL=$(wget -O - http://www.reddit.com/r/$SUBREDDIT.rss | grep -Eo "https://?[^&]+jpg" | grep -v "thumbs" | head -1);
NAME=$(basename "$URL");

if [ "$DIR" ]
  then
    FILENAME="$DIR""$NAME";
  else
    FILENAME=~/Pictures/"$NAME";
fi

wget "$URL" -O "$FILENAME";

echo "$FILENAME";
