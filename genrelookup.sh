#!/bin/bash

rm /tmp/tvinfo

old_IFS=$IFS
IFS=$'\n'

files=( `cat /tmp/tvsorted`)

#IFS=$old_IFS

echo "about to iterate over tvshows"

# Make some links
for f in "${files[@]}"
do
#  echo $f

  info=( `wget --quiet "http://www.omdbapi.com/?t=$f&r=XML&apikey=64eb195f" -O - | xpath -q -e 'concat(string(/root/movie/@genre), "|", string(/root/movie/@plot)' `)
  echo $info
  echo "$f|$info" >> /tmp/tvinfo

done

IFS=$old_IFS

