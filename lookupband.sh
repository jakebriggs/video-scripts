#!/bin/bash

artistid=`curl -s "http://musicbrainz.org/ws/2/artist?query=$1&fmt=json" | jq '.artists[0].id'`

temp="${artistid%\"}"
artistid="${temp#\"}"

echo " -- artistid for $1 is $artistid"

albumid=`curl -s "http://musicbrainz.org/ws/2/artist/$artistid?inc=release-groups&fmt=json"  | jq ".[\"release-groups\"][] | select(.title | contains(\"$2\"))? | .id"`

temp="${albumid%\"}"
albumid="${temp#\"}"

echo " -- albumid for $2 is $albumid"

tags=`curl -s "http://musicbrainz.org/ws/2/release-group/$albumid?inc=tags&fmt=json" | jq '.tags[] | "/bin/ln -s \"/tmp/xyz\" \"/tmp/\(.name | @sh)\" " ' | /bin/bash`


echo $tags

tagssed=`echo $tags > sed s/" "/"-"/g`

echo $tagssed

