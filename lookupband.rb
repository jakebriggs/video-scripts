#!/usr/bin/ruby

require 'faraday'
require 'pry'
require 'json'
require "pathname"
require "set"
require 'fileutils'

args = Hash[ ARGV.map{|s| s.split('=') } ]

useragent="SymlinkTagMaker/1 ( jake@jakebriggs.com )"

localartists = Pathname.new("#{args['--musicpath']}").children.select { |c| c.directory? }

localartists.each { |artist|
        puts ""
	puts "Checking #{artist}"
	puts "----------------------"

	localalbums = artist.children.select { |c| c.directory? }
	
	conn = Faraday.new("http://musicbrainz.org/ws/2/artist?query=#{artist.basename.to_s}&fmt=json")
	conn.headers[:user_agent] = useragent
	response = conn.get

	artistinfo = JSON.parse response.body
	next if artistinfo['artists'].length == 0
	
	artistid = artistinfo['artists'].first['id']
	
	puts "id for #{artist.basename.to_s} is #{artistid}"

	conn = Faraday.new("http://musicbrainz.org/ws/2/artist/#{artistid}?inc=release-groups tags&fmt=json")
        conn.headers[:user_agent] = useragent
	response = conn.get

	albumsinfo = JSON.parse(response.body)['release-groups']
	artisttags = JSON.parse(response.body)['tags']
	tags_to_make = Set[]

	# go over local albums, see if they match anything from musicbrainz and if they do add the tags to the list of tags from the artist
	# then make all the symlinks based on the tags
	localalbums.each { |pn|

          puts ""
	  puts "Checking #{pn.basename.to_s}"
          puts "----------------------"

	  tags_to_make.clear
	  artisttags.each { |at| tags_to_make << at['name'] }
	  
	  albumsinfo.each { |rg|
	    if rg['title'].casecmp(pn.basename.to_s).zero?
              conn = Faraday.new("http://musicbrainz.org/ws/2/release-group/#{rg['id']}?inc=tags&fmt=json")
              conn.headers[:user_agent] = useragent
              response = conn.get 	

	      rginfo = JSON.parse response.body
	      puts "http://musicbrainz.org/ws/2/release-group/#{rg['id']}?inc=tags&fmt=json"
	      rginfo['tags'].each { |t| tags_to_make << t['name'] }
	      sleep 0.2
	    end
	  }

	  puts "Tags for album #{pn.basename.to_s} are:"
          tags_to_make.each { |t|
            puts t
            FileUtils.mkdir_p "#{args['--tagspath']}/#{t}/"
            FileUtils.ln_s pn.to_s, "#{args['--tagspath']}/#{t}/#{pn.basename.to_s}" unless File.exists? "#{args['--tagspath']}/#{t}/#{pn.basename.to_s}" 
          }

	  puts "No tags for #{artist.basename.to_s} - #{pn.basename.to_s}" if tags_to_make.length == 0  
	}
	

}
